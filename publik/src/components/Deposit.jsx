import React, { Component } from 'react';
import {
  isSignInPending,
  loadUserData,
  Person,
  getFile,
  putFile,
  lookupProfile,
} from 'blockstack';

const avatarFallbackImage = 'https://s3.amazonaws.com/onename/avatar-placeholder.png';

export default class Deposit extends Component {

  constructor(props) {
    super(props);

    this.state = {
      person: {
        name() {
          return 'Anonymous';
        },
        avatarUrl() {
          return avatarFallbackImage;
        },
      },
      username: "",
      newStatus: "",
      statuses: [],
      statusIndex: 0,
      isLoading: false
    };
  }

  render() {
    const { handleSignOut } = this.props;
    const { person } = this.state;
    const { username } = this.state;

    return (
      !isSignInPending() && person ?
        <div className="container">
          <div className="row">
            <div className="col-md-offset-3 col-md-6">
              <div className="col-md-12">
              <h1>Deposit</h1>
                <div className="username">
                  <span>{username}</span>
                  {this.isLocal() &&
                    <span>
                      &nbsp;|&nbsp;
                    <a onClick={handleSignOut.bind(this)}>(Logout)</a>
                    </span>
                  }
                </div>
              </div>
              <h4>Select Investment</h4>
              <div>
                <Dropdown list={["11466 King Rd","321 Main Street","85 Danset Street"]} />
              </div>
              {this.isLocal() &&
                <div className="new-status">
                  <div className="col-md-12">
                    <textarea className="input-status"
                      value={this.state.newStatus}
                      onChange={e => this.handleNewStatusChange(e)}
                      placeholder="What's on your mind?"
                    />
                  </div>
                  <div className="col-md-12 text-right">
                    <button
                      className="btn btn-primary btn-lg"
                      onClick={e => this.handleNewStatusSubmit(e)}
                    >
                      Submit
                  </button>
                  </div>
                </div>
              }
              <div className="col-md-12 statuses">
                {this.state.isLoading && <span>Loading...</span>}
                {this.state.statuses.map((status) => (
                  <div className="status" key={status.id}>
                    {status.text}
                  </div>
                )
                )}
              </div>
            </div>
          </div>
        </div> : null
    );
  }


  componentWillMount() {
    this.setState({
      person: new Person(loadUserData().profile),
      username: loadUserData().username
    });
  }

  handleNewStatusChange(event) {
    this.setState({ newStatus: event.target.value })
  }

  handleNewStatusSubmit(event) {
    this.saveNewStatus(this.state.newStatus)
    this.setState({
      newStatus: ""
    })
  }

  saveNewStatus(statusText) {
    let statuses = this.state.statuses

    let status = {
      id: this.state.statusIndex++,
      text: statusText.trim(),
      created_at: Date.now()
    }

    statuses.unshift(status)
    const options = { encrypt: false }
    putFile('statuses.json', JSON.stringify(statuses), options)
      .then(() => {
        this.setState({
          statuses: statuses
        })
      })
  }


  fetchData() {
    this.setState({ isLoading: true })
    if (this.isLocal()) {
      const options = { decrypt: false }
      getFile('statuses.json', options)
        .then((file) => {
          var statuses = JSON.parse(file || '[]')
          this.setState({
            person: new Person(loadUserData().profile),
            username: loadUserData().username,
            statusIndex: statuses.length,
            statuses: statuses,
          })
        })
        .finally(() => {
          this.setState({ isLoading: false })
        })
    } else {
      const username = this.props.match.params.username

      lookupProfile(username)
        .then((profile) => {
          this.setState({
            person: new Person(profile),
            username: username
          })
        })
        .catch((error) => {
          console.log('could not resolve profile')
        })
      const options = { username: username, decrypt: false }
      getFile('statuses.json', options)
        .then((file) => {
          var statuses = JSON.parse(file || '[]')
          this.setState({
            statusIndex: statuses.length,
            statuses: statuses
          })
        })
        .catch((error) => {
          console.log('could not fetch statuses')
        })
        .finally(() => {
          this.setState({ isLoading: false })
        })


    }
  }




  componentDidMount() {
    this.fetchData()
  }

  isLocal() {
    return this.props.match.params.username ? false : true
  }

}
