import React, { Component, Link } from 'react';
import Profile from './Profile.jsx';

//Test page to play around with, Load by changing the Profile call in Render to Profile Test
import ProfileTest from './ProfileTest.jsx';

//Component that contains all Projects
import Overall from './Overall.jsx';

//Component that contains an individual Project
import Project from './Project.jsx';

//Component that contains the deposit form
import Deposit from './Deposit.jsx';

import Signin from './Signin.jsx';
import {
  isSignInPending,
  isUserSignedIn,
  redirectToSignIn,
  handlePendingSignIn,
  signUserOut,
} from 'blockstack';
import { Switch, Route, Redirect } from 'react-router-dom';

var userState = {"inital":1, "profile":2, "overall":3, "project":4, "deposit":5}
//var currentState = userState.profile;

export default class App extends Component {



  constructor(props) {
    super(props);
    this.currentState = userState.overall;
  }

  handleSignIn(e) {
    e.preventDefault();
    const origin = window.location.origin;
    redirectToSignIn(origin, origin + '/manifest.json', ['store_write', 'publish_data']);
    
  }

  handleSignOut(e) {
    e.preventDefault();
    signUserOut(window.location.origin);
  }

  render() {

    if ( !isUserSignedIn() ){

      //User is not yet signed in, Show login Button
      return (
        <div className="site-wrapper">
          <div className="site-wrapper-inner">
            {<Signin handleSignIn={ this.handleSignIn } />}
          </div>
        </div>
      );

    } else {

      //Initial or Profile, route to Profile
      if (this.currentState == userState.profile || this.currentState == userState.inital) {

        return (
          <div className="site-wrapper">
            <div className="site-wrapper-inner">
              {<Switch>
                <Route path='/' 
                component={() => window.location = 'http://localhost:80/realestate-blockstack/app/profile'}/>
                <Route
                  path='/:profile?'
                  render={
                    routeProps => <Overall handleSignOut={this.handleSignOut} {...routeProps} />
                  } />
              </Switch>}
            </div>
            
          </div>
        );

      } else if (this.currentState == userState.overall){
        //Route to Overall
        return (
          <div className="site-wrapper">
            <div className="site-wrapper-inner">
              {<Switch>
                <Route
                  path='/:username?'
                  render={
                    routeProps => <Overall handleSignOut={this.handleSignOut} {...routeProps} />
                  } />
              </Switch>}
            </div>
          </div>
        );

      } else if (this.currentState == userState.project){
        //Route to Project
        return (
          <div className="site-wrapper">
            <div className="site-wrapper-inner">
              {<Switch>
                <Route
                  path='/:username?'
                  render={
                    routeProps => <Project handleSignOut={this.handleSignOut} {...routeProps} />
                  } />
              </Switch>}
            </div>
          </div>
        );

      } else if (this.currentState == userState.deposit){
        //Route to deposit
        return (
          <div className="site-wrapper">
            <div className="site-wrapper-inner">
              {<Switch>
                <Route
                  path='/:username?'
                  render={
                    routeProps => <Deposit handleSignOut={this.handleSignOut} {...routeProps} />
                  } />
              </Switch>}
            </div>
          </div>
        );

      }

    }
  }

  componentWillMount() {
    if (isSignInPending()) {
      handlePendingSignIn().then((userData) => {
        window.location = window.location.origin;
      });
    }
  }

  changeCurrentState(){
    //this.currentState = 4;
  }
}
